<link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Krona+One">
<div class="title">
    <span id="alex">Alex Steinert</span>
    <p id="subtitle">The narcissism is crazy.</p>
    <ul id="tabs">
        <li><a href="www.google.com">Google</a></li>
        <li><a href="www.twitter.com">Twitter</a></li>
        <li><a href="www.facebook.com">Facebook</a></li>
    </ul>
</div>


<style>
    body {
        font-family: "Krona One";
    }

    .title {
        position: absolute;
        width: 50%;
        height: 50%;
        top: 30%;
        left: 32%;
        margin-left: -50px; /* margin is -0.5 * dimension */
        margin-top: -25px;
    }

    #alex {
        font-size: 100px;
    }
    #subtitle {
        margin-left: 30%
    }

    #tabs {
        margin-left: 25%;
    }

    ul {
        list-style-type: none;
        margin: 0;
        padding: 0;
        overflow: hidden;
    }

    li {
        float: left;
    }

    li a {
        display: block;
        color: mediumpurple;
        text-align: center;
        padding: 16px;
        text-decoration: none;
    }

    a:hover {
        background-color: #b8db70;
        transition: all 1s ease;
        -webkit-transition: all 1s ease;
    }
</style>

